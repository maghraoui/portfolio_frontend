import React from "react";
import Project from './types/Project'

const ProjectCard = ({project}) =>{
  return (
    <div>
      <p>{project.name}</p>
      <p>{project.description}</p> 
    </div>
    );
  }

  export default ProjectCard