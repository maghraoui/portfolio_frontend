// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// import { getAnalytics } from "firebase/analytics";
import { getFirestore } from "@firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
// TODO use .env file 
const firebaseConfig = {
  apiKey: "AIzaSyC1wpsi_Bbftm3E46mkDzWrbf6JE87zvnk",
  authDomain: "farid-maghraoui-portfolio.firebaseapp.com",
  projectId: "farid-maghraoui-portfolio",
  storageBucket: "farid-maghraoui-portfolio.appspot.com",
  messagingSenderId: "1038155579826",
  appId: "1:1038155579826:web:e98d89c53b9cc9055da4ab",
  measurementId: "G-HJS7V8BR7B"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// const analytics = getAnalytics(app);

export const db  = getFirestore(app);



