import { useState, useEffect } from 'react'
import * as ReactDOM from 'react-dom';
import reactLogo from './assets/react.svg'
import './App.css'
import {db} from './firebase-config'
import { getDocs, collection } from "firebase/firestore"; 
import axios from 'axios';
 
import ProjectCard from './projectCard';
import RandomJsonData from './RandomJsonData'

import Header from './components/header/Header' 
import Nav from './components/nav/Nav'
import About from './components/about/About' 
import Experience from './components/experience/Experience';
import Services from './components/services/Services';
import Portfolio from './components/portfolio/Portfolio';
import Contact from './components/contact/Contact';
import Footer from './components/footer/Footer';

const PROJECTS_COLLECTION = "projects"
const URL_JSON ="https://jsonplaceholder.typicode.com/todos/"


function randomNumberInRange(min: number, max: number) {
  // 👇️ get number between min (inclusive) and max (inclusive)
  return Math.floor(Math.random() * (max - min + 1)) + min;
}


function App() {
  // const [userId, setUserId ] = useState(1)
  // const [projects, setProjects ] = useState([])
  // const [jsonData, setJsonData ] = useState({})
  // const projectsCollectionRef = collection(db, PROJECTS_COLLECTION)
  // useEffect( ()=>{

  //   const getProjects = async ()=>{
  //     const data = await getDocs(projectsCollectionRef);
  //     setProjects(data.docs.map((doc) => ({...doc.data()})));  
  //   }
  //   getProjects()

  // }, [])
  // const fetchDataJson = () => {
  //   let rando = randomNumberInRange(1,10);
  //   setUserId(rando);
  //   console.log(userId)
  //   axios.get(`${URL_JSON}/${userId}`).then((resp) => {
  //     console.log(resp.data);
  //     setJsonData(resp.data);
  //     });
  
  // }
  // useEffect( ()=>{
  //   fetchDataJson();
  // }, [])
  
  return (
    <>
      <Header />
      <Nav />
      <About />
      <Experience />
      <Services />
      <Portfolio />
      <Contact />
      <Footer />

    </>
    // TODO : project component!
    // <div className="App">
    //   {projects?.length > 0  ? (
    //         <div className="container">
    //          {projects.map((project:any)=>
    //          (<ProjectCard project={project}/>)
             
    //          )} 
    //          </div>
    //        ): (
    //         <div className="empty">
    //           <h2>No movies found</h2>
    //         </div>
    //       )
          
    //     }
       
    //   <button onClick={(event) =>{event.stopPropagation();fetchDataJson()}}>fetch me</button>
      
    //   <div className="append_div_json">
    //     <RandomJsonData datajson={jsonData}/>
    //     </div>
    // </div>
  )
}
export default App
