import React from 'react';
import { AiOutlineAlert, AiOutlineMail } from 'react-icons/ai';
import { BsLinkedin } from 'react-icons/bs';
import './contact.css';
const Contact = () => {
    return (
        <section id='contact'>
            <h5>Get In Touch</h5>
            <h2>Contact Me</h2>
            <div className="container contact__container">
                <div className="contact__options">
                    <article className="contact__option">
                        <AiOutlineMail />
                        <h4>Email</h4>
                        <h5>ferid.maghraoui@gmail.com</h5>
                        <a href="mailto:ferid.maghraoui@gmail.com">Send a message</a>

                    </article>

                    <article className="contact__option">
                        <BsLinkedin />
                        <h4>Linkedin</h4>
                        <h5>Farid Maghraoui</h5>
                        <a href="https://www.linkedin.com/in/farid-maghraoui/" target={'_blank'}>Send a message</a>
                    </article>

                    <article className="contact__option">
                        <BsLinkedin />
                        <h4>WhatsApp</h4>
                        <h5>+491522367..</h5>
                        <a href="https://web.whatsapp.com/send?phone+4915223675568" target={'_blank'}>Send a message</a>
                    </article>
                </div>
                <form action="">
                    <input type="text" name='name' placeholder='Your Full Name' required />
                    <input type="text" name="email" placeholder='Your Email' required />
                    <textarea name="message" id="message" placeholder='Your Message' required></textarea>
                    <button type='submit' className='btn  btn-primary'>Send Message</button>
                </form>
            </div>
            {/* END OF CONTACT DETAILS */}

        </section>
    );
};

export default Contact;