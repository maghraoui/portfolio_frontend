import React from 'react';
import { BiCheck } from 'react-icons/bi';
import './services.css'

const Services = () => {
    return (
        <section id='services'>
            <h5>What I offer</h5>
            <h2>Services  </h2>

            <div className="container services__container">
                {/* TODO Component */}
                <article className="service">
                    <div className="service__head">
                        <h3>UI Design</h3>
                    </div>
                    <ul className="service__list">
                        <li>
                            <BiCheck className='service__list-item' />
                            <p>lorem lorem lorem...</p>
                        </li>
                        <li>
                            <BiCheck className='service__list-item' />
                            <p>lorem lorem lorem...</p>
                        </li>
                        <li>
                            <BiCheck className='service__list-item' />
                            <p>lorem lorem lorem...</p>
                        </li>
                        <li>
                            <BiCheck className='service__list-item' />
                            <p>lorem lorem lorem...</p>
                        </li>
                    </ul>

                </article>
                {/* END OF UI/UX */}
                <article className="service">
                    <div className="service__head">
                        <h3>Web Development</h3>
                    </div>
                    <ul className="service__list">
                        <li>
                            <BiCheck className='service__list-item' />
                            <p>lorem lorem lorem...</p>
                        </li>
                        <li>
                            <BiCheck className='service__list-item' />
                            <p>lorem lorem lorem...</p>
                        </li>
                        <li>
                            <BiCheck className='service__list-item' />
                            <p>lorem lorem lorem...</p>
                        </li>
                        <li>
                            <BiCheck className='service__list-item' />
                            <p>lorem lorem lorem...</p>
                        </li>
                    </ul>

                </article>
                {/* END OF Web Development */}
                <article className="service">
                    <div className="service__head">
                        <h3>Rest API Design/Development</h3>
                    </div>
                    <ul className="service__list">
                        <li>
                            <BiCheck className='service__list-item' />
                            <p>lorem lorem lorem...</p>
                        </li>
                        <li>
                            <BiCheck className='service__list-item' />
                            <p>lorem lorem lorem...</p>
                        </li>
                        <li>
                            <BiCheck className='service__list-item' />
                            <p>lorem lorem lorem...</p>
                        </li>
                        <li>
                            <BiCheck className='service__list-item' />
                            <p>lorem lorem lorem...</p>
                        </li>
                    </ul>

                </article>
            </div>
        </section>
    );
};

export default Services;