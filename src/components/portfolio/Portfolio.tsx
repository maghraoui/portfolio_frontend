import React from 'react';
import './portfolio.css'
import IMG1 from '../../assets/me.png'

const data_portfolio = [
    {
        id: 1,
        image: IMG1,
        title: 'blabla',
        github: 'https://github.com',
        demo: 'https://github.com'
    },
    {
        id: 2,
        image: IMG1,
        title: 'blabla',
        github: 'https://github.com',
        demo: 'https://github.com'
    },
    {
        id: 3,
        image: IMG1,
        title: 'blabla',
        github: 'https://github.com',
        demo: 'https://github.com'
    },
    {
        id: 4,
        image: IMG1,
        title: 'blabla',
        github: 'https://github.com',
        demo: 'https://github.com'
    },
    {
        id: 5,
        image: IMG1,
        title: 'blabla',
        github: 'https://github.com',
        demo: 'https://github.com'
    },
    {
        id: 6,
        image: IMG1,
        title: 'blabla',
        github: 'https://github.com',
        demo: 'https://github.com'
    }
]

const Portfolio = () => {
    return (
        <section id='portfolio'>
            <h5>My Recent Work</h5>
            <h2>Portfolio</h2>
            <div className="container portfolio__container">
                {/* TODO compononent */}

                {
                    data_portfolio.map(({id, image,title, github, demo}) => {
                        return (
                            <article key={id} className="portfolio__item">
                    <div className="portfolio__item-image">
                        <img src={image} alt={title} />
                    </div>
                    <h3>{title}</h3>
                    <div className="portfolio__item-cta">
                        <a href={github} className="btn">Github</a>
                        <a href={demo} target={'_blank'} className='btn btn-primary'>Live demo</a>
                    </div>
                </article>
                        )

                    })
                }

            </div>
        </section>
    );
};

export default Portfolio;